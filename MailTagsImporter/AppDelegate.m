//
//  AppDelegate.m
//  MailTagsImporter
//
//  Created by Scott Morrison on 28/08/2018.
//  Copyright © 2018 SmallCubed. All rights reserved.
//

#import "AppDelegate.h"
#import "MKTagImporter.h"
@interface AppDelegate ()

@property (weak) IBOutlet NSWindow *window;
@end

@implementation AppDelegate

- (void)writeMKTagFile{
    // write out the tag file to the expected directory
    
    NSString * path= [[NSBundle bundleForClass:[self class]] pathForResource:@"7119gVv8swVE3WGwyq" ofType:@"MKTag"];
    NSData * jsonData = [NSData dataWithContentsOfFile:path];
    NSString * tmpJsonFolderPath = [@"~/Library/SmallCubed/TagData/BNcu5purqP59/7/1/1/9/" stringByStandardizingPath];
    [[NSFileManager defaultManager] createDirectoryAtPath:tmpJsonFolderPath withIntermediateDirectories:YES attributes:0 error:nil];
    NSString * tmpJsonPath =[tmpJsonFolderPath stringByAppendingPathComponent:@"7119gVv8swVE3WGwyq.MKTag"];
    [jsonData writeToFile:tmpJsonPath atomically:YES];
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    
    // attempt to read code directly from emlx file's plist
    [self writeMKTagFile];
    
    NSString * path= [[NSBundle bundleForClass:[self class]] pathForResource:@"105072" ofType:@"emlx"];

    MKImportedTags * tags = [MKImportedTags importedTagsForEmlxFile: path];
    NSLog (@"tagKeywords: %@",tags.keywords);
    
    if ((0)){
        //
        
                                 
        NSString * fullIdentifier = [MKImportedTags tagIdentifierFromEmlxFile:path];
        
        if (!fullIdentifier){
            // emlx plist did not have identifier == so fall back on generating from headers;
            MKMessageHeaders * headers = [MKMessageHeaders headersFromEmlxFileAtPath:path];
            if ([headers.tagMessageIdentifier isEqualToString:@"7119gVv8swVE3WGwyq"]){
                NSLog(@"Its a miracle!");
            }
            else{
                NSLog(@"Shit!");
            }
            
            MKImportAccount * acct = [[MKImportAccount alloc] initWithAccountLoginName:@"smorr@smallcubed.com" hostName:@"mail.hover.com"];
            
            NSString * fullIdentifier = [headers tagIdentifierForImportAccount:acct];
            if ([fullIdentifier isEqualToString:@"cnPdomBNcu5purqP59/7119gVv8swVE3WGwyq"]){
                NSLog(@"Its a miracle Again!");
            }
            else{
                NSLog(@"Shit!");
            }
        }
    }
    
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}


@end
