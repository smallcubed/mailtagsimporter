//
//  AppDelegate.h
//  MailTagsImporter
//
//  Created by Scott Morrison on 28/08/2018.
//  Copyright © 2018 SmallCubed. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

