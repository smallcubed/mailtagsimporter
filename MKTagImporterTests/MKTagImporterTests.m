//
//  MKTagImporterTests.m
//  MKTagImporterTests
//
//  Created by Scott Morrison on 28/08/2018.
//  Copyright © 2018 SmallCubed. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MKTagImporter.h"

@interface MKTagImporterTests : XCTestCase

@end

@implementation MKTagImporterTests

- (void)setUp {
    
   
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
}

- (void)testReadFromEmlxPlist{
    NSString * path= [[NSBundle bundleForClass:[self class]] pathForResource:@"105072" ofType:@"emlx"];
    NSString * identifier = [MKImportedTags tagIdentifierFromEmlxFile:path];
    XCTAssert([identifier isEqualToString:@"BNcu5purqP59/7119gVv8swVE3WGwyq"],@"wrong identifier" );

}
- (void)testReadFromHeaders {
    NSString * path= [[NSBundle bundleForClass:[self class]] pathForResource:@"105072" ofType:@"emlx"];
    MKMessageHeaders * headers = [MKMessageHeaders headersFromEmlxFileAtPath:path];
    XCTAssert([headers.tagMessageIdentifier isEqualToString:@"7119gVv8swVE3WGwyq"],@"wrong identifier" );
}
-(void)testReadTags{
    // write out sample data to an appropriate temp path
    NSString * path= [[NSBundle bundleForClass:[self class]] pathForResource:@"7119gVv8swVE3WGwyq" ofType:@"MKTag"];
    NSData * jsonData = [NSData dataWithContentsOfFile:path];
    NSString * rootPath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"MKTagImportTest"];
    NSString * tmpJsonFolderPath = [rootPath stringByAppendingPathComponent:@"/BNcu5purqP59/7/1/1/9/"];
    [[NSFileManager defaultManager] createDirectoryAtPath:tmpJsonFolderPath withIntermediateDirectories:YES attributes:0 error:nil];
    NSString * tmpJsonPath =[tmpJsonFolderPath stringByAppendingPathComponent:@"7119gVv8swVE3WGwyq.MKTag"];
    [jsonData writeToFile:tmpJsonPath atomically:YES];
    
    NSString * emlxPath= [[NSBundle bundleForClass:[self class]] pathForResource:@"105072" ofType:@"emlx"];
    
    MKImportAccount * acct = [[MKImportAccount alloc] initWithAccountLoginName:@"smorr@indev.ca" hostName:@"homie.mail.dreamhost.com"];
    MKMessageHeaders * headers = [MKMessageHeaders headersFromEmlxFileAtPath:emlxPath];
    NSString * fullIdentifier = [headers tagIdentifierForImportAccount:acct];
     MKImportedTags * tags = [[MKImportedTags alloc] initWithJsonFileForTagIdentifier:fullIdentifier relativeToPath:rootPath];
    NSArray * expectedArray = @[@"foo",@"bar"];
    
    XCTAssert([tags.keywords isEqualToArray:expectedArray],@"wrong keywords" );

    
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
