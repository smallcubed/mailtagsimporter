//
//  NSString+MimeEncoding.m
//  EmailAddressParser
//
//  Created by smorr on 2015-01-15.
//  Copyright (c) 2015 Indev Software. All rights reserved.
//

/*
 Portions of code are taken from http://stackoverflow.com/a/14014839/922658
 No licence information provided.
 */

#if ! __has_feature(objc_arc)
#error This file must be compiled with ARC. Use -fobjc-arc flag (or convert project to ARC).
#endif

#import "NSString+MimeEncoding.h"
#import "NSData+MimeWordDecoding.h"
#include <CommonCrypto/CommonCrypto.h>

@implementation NSString (MimeEncoding)
-(NSString*)shortSHAHashString{
#define ID_SIZE 12
    // Size 12 = 72 bits = number of inputs to have a 50% chance of collision: 8.09*10^10 (80.9 billion)
    // number of input will be much greater once there are constraints to the inputs.
    
    const char *s=[self cStringUsingEncoding:NSUTF8StringEncoding];
    
    uint8_t digest[CC_SHA256_DIGEST_LENGTH]={0};
    CC_SHA256(s, (CC_LONG)strlen(s), digest);
    
    NSData * digestData = [NSData dataWithBytes:digest length:CC_SHA256_DIGEST_LENGTH];
    
    NSString * generatedID =[[digestData pathSafeBase64EncodedString] substringToIndex:ID_SIZE];
    return generatedID;
    
}

-(NSString*)decodedMimeEncodedString{
    NSMutableString * decodedString = [NSMutableString string];
    @autoreleasepool {
        
        NSScanner * scanner = [NSScanner scannerWithString:self];
        [scanner setCharactersToBeSkipped:nil];
        [scanner setCaseSensitive:NO];
        NSMutableData * fullDecodedData = [NSMutableData data];
        NSStringEncoding dataEncoding = 0;
        
        while (![scanner isAtEnd]){
            NSString * leadingWhiteSpace = nil;
            [scanner scanCharactersFromSet:[NSCharacterSet whitespaceAndNewlineCharacterSet] intoString:&leadingWhiteSpace];
            
            NSString * leadingString = nil;
            [scanner scanUpToString:@"=?" intoString:&leadingString];
 
            
            NSUInteger mimeStart = [scanner scanLocation];
            NSUInteger mimeEnd = NSNotFound;
            if ([scanner scanString:@"=?" intoString:nil]){
                if (leadingString) {
                    // we hit another encoding start just after the last encoded word -- so keep accumulating as if it were one word.
                    if (leadingWhiteSpace) [decodedString appendString:leadingWhiteSpace];
                    [decodedString appendString:leadingString];
                }
                [scanner scanUpToString:@"?" intoString:nil];  // scan the encoding
                if ([scanner scanString:@"?" intoString:nil]){
                    if ([scanner scanString:@"B" intoString:nil] || [scanner scanString:@"Q" intoString:nil]){  // scans type
                        if ([scanner scanString:@"?" intoString:nil]){
                            [scanner scanUpToString:@"?=" intoString:nil];  // scans word
                            if ([scanner scanString:@"?=" intoString:nil])
                                mimeEnd = [scanner scanLocation];
                        }
                    }
                }
            }
            else{
                // we did not hit another encoding start, so append the last data chunk and continue.
                if (fullDecodedData && dataEncoding){
                    NSString * decodedChunk =  [[NSString alloc] initWithData:fullDecodedData encoding:dataEncoding];
                    if (decodedChunk){
                        [decodedString appendString: decodedChunk];
                    }
                    fullDecodedData = [NSMutableData data];
                    dataEncoding = 0;
                }

                if (leadingString) {
                    if (leadingWhiteSpace) [decodedString appendString:leadingWhiteSpace];
                    [decodedString appendString:leadingString];
                }
            }
            if (mimeEnd != NSNotFound){
                NSString * mimeWord = [[scanner string] substringWithRange:NSMakeRange(mimeStart, mimeEnd-mimeStart)];
                NSStringEncoding wordEncoding = 0;
                NSData * decodedWordData = nil;
                @try {
                    decodedWordData = [NSData dataForMimeEncodedWord:mimeWord usedEncoding:&wordEncoding];
                }
                @catch (NSException *exception) {
                    NSLog (@"problem parsing MimeEncodedWord: %@",mimeWord);
                }
                @finally {
                    
                }
                
                if (dataEncoding==0){
                    // this is first pass -- just append the data;
                    dataEncoding = wordEncoding;
                    [fullDecodedData appendData:decodedWordData];
                }
                else if (wordEncoding != dataEncoding){
                    // this word has a different encoding than accumulated data
                    // so convert the accumulated data to a string (using its encoding) and append to ongoing string.
                    
                    NSString * decodedChunk =  [[NSString alloc] initWithData:fullDecodedData encoding:dataEncoding];
                    if (decodedChunk){
                        [decodedString appendString: decodedChunk];
                    }
                    else{
                        NSLog(@"problem decoding MimeString: %@",self);
                    }
                    // and start accumulating data again with new encoding.
                    dataEncoding = wordEncoding;
                    [fullDecodedData setData:decodedWordData];
                }
                else{
                    // same encoding as last chuck -- so just append the data.
                    [fullDecodedData appendData:decodedWordData];
                }
            }
        
        
            // if there is is any remaining accumulated data, decode into string and append to the wholeString
            
        }
        if (fullDecodedData && dataEncoding){
            NSString * decodedChunk =  [[NSString alloc] initWithData:fullDecodedData encoding:dataEncoding];
            if (decodedChunk){
                [decodedString appendString: decodedChunk];
            }
        }
    }
    return [NSString stringWithString:decodedString];
}

@end

