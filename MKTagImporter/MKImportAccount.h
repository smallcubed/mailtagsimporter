//
//  MKImportAccount.h
//  MKTagImporter
//
//  Created by Scott Morrison on 28/08/2018.
//  Copyright © 2018 SmallCubed. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/* MKImportAccount
 
 Tags are discrete records in the context of a mail account.  All instances of a message in a mail account (regardless of containing mailbox will share the same tag record.
 
 To Identify Mail accounts we simply concatenate the loginName and the hostname of the account and hash the value (and truncate it.
 
 In 10.14 Mail stores messages in ~/Library/Mail/V6/ subfolders that are identified by account UUIDS.   This uuid is arbitrary and will change from machine to machine -- we need to use information that will produce a constant identifier.
 
 However you can use AppleScript to query Mail about accounts with uuids.
        -- returns space delimited string of user name and host name of account ~/Library/Mail/V6/50DF15A7-998A-42E0-A936-AD468278CC2B
 
        tell application "Mail"
            set acctList to (every account whose id is "50DF15A7-998A-42E0-A936-AD468278CC2B")
            if (count of acctList) = 1 then
                set acct to item 1 of acctList
                get user name of acct & " " & server name of acct
            end if
        end tell
 
Local accounts are a little more tricky
        you can't get the user name/hostname of a local account but you can test to see if a uuid corresponds to a local account by
 
        get the container of item 1 of every mailbox
 
        unfortunately this produces an Applescript reference that cannot be further queried.
 
        But with AEDescriptors in objc you can extract the id used even if you can't retreive it in applescript itself. (gotta love applescript!)
 
        NSAppleScript * script = [[NSAppleScript alloc] initWithSource:@"tell Application \"Mail\" to get container of item 1 of every mailbox"];
        NSDictionary * asError = nil;
        NSAppleEventDescriptor   * result =  [script executeAndReturnError:&asError];
 
        if ([result isRecordDescriptor] ){
        NSString * localAccountId =  [[result descriptorForKeyword:'seld'] stringValue];
 
        For the local account we are currently using loginName of "" and hosthame of "(null)"
 
        We are planning to change this however.
 
 
 In any case you can create an simple MKImportAccount object using either a tagAccountIdentifier or a loginname/hostname
 
 */

@interface MKImportAccount : NSObject
@property (strong) NSString * tagAccountIdentifier;

-(instancetype)initWithTagAccountIdentifier:(NSString*)identifier;
-(instancetype)initWithAccountLoginName:(NSString*)loginName hostName:(NSString*)hostName;

@end

NS_ASSUME_NONNULL_END
