//
//  MKMessageHeaders.m
//  Martlet
//
//  Created by Scott Morrison on 11-01-30.
//  Copyright 2011 Indev Software, Inc. All rights reserved.
//

#import "MKMessageHeaders.h"

#import "NSString+MimeEncoding.h"
#import "NSData+MimeWordDecoding.h"
#import "MKImportAccount.h"
#include <CommonCrypto/CommonCrypto.h>

@interface MKMessageHeaders()
@property (readwrite,strong) NSDictionary * headerDictionary;
@property (strong) NSString * _tagMessageIdentifier_;

@end


static NSUInteger MKGenerateHashPrefix(NSString * string) {
    // return a 4 digit hash of a string.
    //algorithm from http://algoviz.org/OpenDSA/Books/OpenDSA/html/HashFuncExamp.html
    
    const char * key = [string UTF8String];
    NSInteger length = strlen(key);
    NSInteger length4 = length/4;
    long long sum = 0;
    for (int j = 0; j < length4; j++) {
        long long mult = 1;
        for (int k = 0; k < 4; k++) {
            sum += key[j*4+k] * mult;
            mult *= 256;
        }
    }
    
    long long mult = 1;
    for (int k = 0; k < length; k++) {
        sum += key[k] * mult;
        mult *= 256;
    }
    return sum %10000;
}






@implementation MKMessageHeaders

- (id)initWithHeaderDictionary:(NSDictionary *)aHeaderDictionary {
    self = [super init];
    
    if (self) {
        self.headerDictionary = aHeaderDictionary;
    }
    
    return self;
}

- (NSString *)firstHeaderForKey:(NSString *)key {
    return [self firstHeaderForKey:key decodeMime:YES];
}

-(NSString *)firstHeaderForKey:(NSString *)key decodeMime:(BOOL)decodeMime{
    for (NSString * aHeaderKey in self.headerDictionary.allKeys) {
        if ([key caseInsensitiveCompare:aHeaderKey] == NSOrderedSame){
            NSString * headerValue = [[self headerDictionary][aHeaderKey] objectAtIndex:0];
            NSString * decodedValue = nil;
            if (decodeMime){
                decodedValue = [headerValue decodedMimeEncodedString];
                if (decodedValue) headerValue = decodedValue;
            }
            headerValue =  [headerValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            return [headerValue stringByReplacingOccurrencesOfString:@"\r\n" withString:@""];
        }
    }
    return nil;
}
+(MKMessageHeaders *)headersFromData:(NSData*)headerData{
    
    // Scan the data into a header dictionary and create a MKHeaders instance with this dictionary
    NSString * headerString = nil;
    // if we don't suggest encodings, it can return some wacky shit
    NSStringEncoding big5 =  (NSStringEncoding)CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingBig5);
    NSStringEncoding big5_E =  (NSStringEncoding)CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingBig5_E);
    NSStringEncoding big5_HKSCS =  (NSStringEncoding)CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingBig5_HKSCS_1999);
    NSStringEncoding HZ_GB_2312 =  (NSStringEncoding)CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingHZ_GB_2312);
    
    NSArray * stringEncodings = @[@(NSNonLossyASCIIStringEncoding),
                                  @(NSASCIIStringEncoding),
                                  @(NSISOLatin1StringEncoding),
                                  @(NSISOLatin2StringEncoding),
                                  @(NSMacOSRomanStringEncoding),
                                  @(NSUTF8StringEncoding),
                                  @(NSUTF16StringEncoding),
                                  @(NSShiftJISStringEncoding),
                                  @(NSJapaneseEUCStringEncoding),
                                  @(NSISO2022JPStringEncoding),  // widely used in japanese email -- 7 bit
                                  @(NSWindowsCP1250StringEncoding),
                                  @(NSWindowsCP1251StringEncoding),
                                  @(NSWindowsCP1252StringEncoding),
                                  @(NSWindowsCP1253StringEncoding),
                                  @(NSWindowsCP1254StringEncoding),
                                  @(NSWindowsCP1250StringEncoding),
                                  @(big5),
                                  @(big5_E),
                                  @(big5_HKSCS),
                                  @(HZ_GB_2312), //HZ (RFC 1842, for Chinese mail & news)
                                  ];
    
    NSStringEncoding encodingUsed = [NSString stringEncodingForData:headerData
                                                    encodingOptions:@{NSStringEncodingDetectionAllowLossyKey:@YES,
                                                                      NSStringEncodingDetectionSuggestedEncodingsKey:stringEncodings,
                                                                      NSStringEncodingDetectionUseOnlySuggestedEncodingsKey:@YES,
                                                                      NSStringEncodingDetectionFromWindowsKey:@YES
                                                                      }
                                                    convertedString:&headerString
                                                usedLossyConversion:nil];
    
    if (!headerString){
        return nil;
    }
    
    NSMutableDictionary * headerDictionary = [NSMutableDictionary dictionary];
    
    static NSString * lineBreak = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        lineBreak = @"\n";
    });
    
    NSScanner * headerScanner = [NSScanner scannerWithString:headerString];
    [headerScanner setCharactersToBeSkipped:nil];
    
    NSString * headerKey = nil;
    NSMutableString * headerValue = nil;
    
    while (![headerScanner isAtEnd]) {
        NSString * headerLine = nil;
        [headerScanner scanUpToString:lineBreak intoString: &headerLine];
        [headerScanner scanString:lineBreak intoString:nil];
        
        if ([headerLine hasPrefix:@"\t"] || [headerLine hasPrefix:@" "]) {
            if (headerValue) {
                [headerValue appendFormat:@" %@", [headerLine stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
            }
        }
        else {
            if (headerKey && headerValue) {
                if (!headerDictionary[headerKey]) {
                    headerDictionary[headerKey] = [NSMutableArray array];
                }
                
                [headerDictionary[headerKey] addObject: [headerValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
            }
            if ([headerLine length]){
                NSScanner * headerKeyScanner = [NSScanner scannerWithString:headerLine];
                [headerKeyScanner scanUpToString:@":" intoString:&headerKey];
                [headerKeyScanner scanString:@":" intoString:nil];
                NSString *tempValue = nil;
                [headerKeyScanner scanUpToString:lineBreak intoString:&tempValue];
                headerValue = [tempValue?:@"" mutableCopy];
            }
        }
    }
    
    if (headerKey && headerValue) {
        if (!headerDictionary[headerKey]) {
            headerDictionary[headerKey] = [NSMutableArray array];
        }
        
        [headerDictionary[headerKey] addObject: [headerValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
    }
    
    
    MKMessageHeaders * headers = [[MKMessageHeaders alloc] initWithHeaderDictionary:headerDictionary];
    headers.preferredEncoding = encodingUsed;
    headers._tagMessageIdentifier_ = [headers _generateTagMessageIdentifier];
    
    return headers;
}
+(MKMessageHeaders *)headersFromEmlxFileAtPath:(NSString*)path{
    if (![[NSFileManager defaultManager] fileExistsAtPath:path]){
        return nil;
    }
    const int bufferSize = 512;
    static NSData * headerBreak = nil;
    if (!headerBreak){
        headerBreak = [NSData dataWithBytes:"\n\n" length:2];
    }
    
    int fd = open([path fileSystemRepresentation],O_RDONLY);
    NSData * headerData = nil;
    while (fd){
        char offsetBuffer[12];
        ssize_t bytes_read = read(fd,offsetBuffer,11);
        if (bytes_read!=11)
        break;  // not enough data in the file to even read an offset??
        
        offsetBuffer[11] = 0;  // null terminate the buffer incase it is not null terminated (this will truncate the buffer by one character)
        
        char *newLinePtr = strchr(offsetBuffer,'\n');
        if (!newLinePtr){
            NSLog(@"can't find end of offset %@",path);
            break;
        }
        // find the length of the offset line
        long len = newLinePtr - offsetBuffer;
        
        // set the file position to the start of the header  and read header data
        
        lseek(fd,len+1,SEEK_SET);
        
        NSMutableData * readData = [[NSMutableData alloc] init];
        char buffer[bufferSize];
        ssize_t bytesRead = 0;
        do{
            bytesRead = read(fd,buffer,bufferSize);
            if (bytesRead>0){
                NSMutableData * bufferData = [[NSMutableData alloc] initWithBytesNoCopy:buffer length:bytesRead freeWhenDone:NO];
                [readData appendData:bufferData];
                NSUInteger dataLength = [readData length];
                // set up search to only cover last read amount + a few extra bytes
                NSInteger searchStart =MAX(0,(NSInteger)(dataLength-bytesRead-5));
                NSUInteger searchLength =dataLength-searchStart;
                NSRange  headersRange = [readData rangeOfData:headerBreak options:0 range:NSMakeRange(searchStart,searchLength)];
                if (headersRange.location!=NSNotFound){
                    headerData =  [readData subdataWithRange:NSMakeRange(0,headersRange.location+headerBreak.length)];
                    break;
                }
            }
        } while (bytesRead>0);
        break;
    }
    close(fd);
    
    return [self headersFromData:headerData];
}
-(NSString *)hashForCompoundedHeaderValueString:(NSString* )baseString{
    if (baseString.length == 0) return nil;
    
    
#define ID_SIZE  14
    uint8_t digest[CC_SHA256_DIGEST_LENGTH]={0};
    BOOL calculatedDigest = NO;
    
    if ([baseString canBeConvertedToEncoding:NSUTF8StringEncoding]){
        // try NSUTF8Encoding first as this is was has been used in legacy code
        // DO NOT resort to other encodings unless absolutely necessary
        const char *s=[baseString cStringUsingEncoding:NSUTF8StringEncoding];
        if (s){
            __unused unsigned char * result =  CC_SHA256(s, (CC_LONG)strlen(s), digest);
            calculatedDigest = YES;
        }
    }
    
    if (!calculatedDigest && [baseString canBeConvertedToEncoding:self.preferredEncoding]){
        // could not generate hash using UTF8 encoding, try the messageHeaders preferred encoding.
        NSData * baseData = [baseString dataUsingEncoding:self.preferredEncoding];
        if (baseData.length){
            CC_SHA256(baseData.bytes, (CC_LONG)baseData.length, digest);
            calculatedDigest = YES;
        }
    }
    if (!calculatedDigest){
        // UTF8encoding and preferredEncoding did not work,try what Foundation things is the fastest encoding.
        NSStringEncoding fastestEncoding = [baseString fastestEncoding];
        if ([baseString canBeConvertedToEncoding:fastestEncoding]){
            NSData * baseData = [baseString dataUsingEncoding:fastestEncoding];
            if (baseData.length){
                CC_SHA256(baseData.bytes, (CC_LONG)baseData.length, digest);
                calculatedDigest= YES;
            }
        }
    }
    if (!calculatedDigest) {
        // no encodings seemed to work -- maybe this is something I just should give up on.
        NSLog(@"Could not generate a hash for headerValues -- encoding issues");
        return nil;
    }
    NSData * digestData = [NSData dataWithBytes:digest length:CC_SHA256_DIGEST_LENGTH];
    NSString * fullDigestString = [digestData pathSafeBase64EncodedString];
    NSString * generatedID =[fullDigestString substringToIndex:ID_SIZE];
    
    NSString * numericHashString = [NSString stringWithFormat:@"%04lu",MKGenerateHashPrefix(fullDigestString)];
    NSString * fullHash = [NSString stringWithFormat:@"%@%@",numericHashString,generatedID];
    return fullHash;
}
-(NSString*)_generateTagMessageIdentifier{
    
    // the tagMessageIdentifier is formed by creating a SHA 256 hash (ShaHash) of
    // the following concatenated headers values (or placeholders if absent)
    //      (subject/message-id/to/from/date-sent/cc/bcc/x-smallcubed-ids)
    // from the full message headers.
    //
    // the x-smallcubed-id header is a reserved value that may be used in the future
    // to differentiate multiple instances of a message on the same account
    //  (ie to force different identifier generation.
    
    // the shaHash is then converted to a url pathsafe ('/' -> '-' and '+' -> '_' ) base 64 representation (the Base64Hash)
    // the Base64Hash is passed through a second "elf" hash function to generate a 4 digit integer (0000-9999) (the KeyPrefix)
    // the final tagMessageIdentifier is a concatenation of an KeyPrefix as a 4 character string  and the first 14 characters of the Base64Hash
    
    // the KeyPrefix "0000"-"9999" is used to both add complexity to the hash and to be decomposed into digits to provide a file path
    
    //eg tagMessageIdentifier key "0748w2mCzPxaeLTUay" becomes file path
    //      "../0/7/4/8/0748w2mCzPxaeLTUay.extension"

    
    NSMutableArray * keyItems = [NSMutableArray array];
    
    [keyItems addObject:[self firstHeaderForKey:MKMessageHeaderToKey decodeMime:NO]?:@"NO_RECIPIENTS"];
    [keyItems addObject:[self firstHeaderForKey:MKMessageHeaderCcKey decodeMime:NO]?:@"NO_CC_RECIPIENTS"];
    [keyItems addObject:[self firstHeaderForKey:MKMessageHeaderBccKey decodeMime:NO]?:@"NO_BCC_RECIPIENTS"];
    [keyItems addObject:[self firstHeaderForKey:MKMessageHeaderFromKey decodeMime:NO]?:@"NO_FROM"];
    
    [keyItems addObject:[self firstHeaderForKey:MKMessageHeaderSubjectKey]?:@"NO_SUBJECT"];
    [keyItems addObject:[self firstHeaderForKey:MKMessageHeaderMessageIdKey]?:@"NO_MESSAGE_ID"];
    [keyItems addObject:[self firstHeaderForKey:MKMessageHeaderDateKey]?:@"NO_DATE"];
    NSString * persistenceHistory = [self firstHeaderForKey:@"x-smallcubed-ids"];
    if (persistenceHistory) [keyItems addObject:persistenceHistory];
    
    // concatenate the parts
    NSString * compoundString = [keyItems componentsJoinedByString:@"|"];
    
    //strip the spaces (sometimes IMAP header requests from a gmail server will return differently formatted headers than if you request the full rfc822 data of a message -- this will result in mismatched keys for same message.   Stripping spaces resolves this.
    static NSRegularExpression * stripSpaces = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        stripSpaces = [NSRegularExpression regularExpressionWithPattern:@"\\s" options:0 error:nil];
    });
    
    compoundString = [stripSpaces stringByReplacingMatchesInString:compoundString options:0 range:NSMakeRange(0,compoundString.length) withTemplate:@""];
    return [self hashForCompoundedHeaderValueString:compoundString];
}

-(NSString*)tagMessageIdentifier{
    NSString * identifier = nil;
    @synchronized(self){
        if (!self._tagMessageIdentifier_){
            self._tagMessageIdentifier_ = [self _generateTagMessageIdentifier];
        }
        identifier =  self._tagMessageIdentifier_;
    }
    return identifier;
}

-(NSString*)tagIdentifierForImportAccount:(MKImportAccount*)account{
    return [NSString stringWithFormat:@"%@/%@",account.tagAccountIdentifier,self._tagMessageIdentifier_];
}

@end


NSString * const MKMessageHeaderDateKey = @"Date";
NSString * const MKMessageHeaderFromKey = @"From";
NSString * const MKMessageHeaderSenderKey = @"Sender";
NSString * const MKMessageHeaderToKey = @"To";
NSString * const MKMessageHeaderCcKey = @"Cc";
NSString * const MKMessageHeaderBccKey = @"Bcc";
NSString * const MKMessageHeaderMessageIdKey = @"Message-Id";
NSString * const MKMessageHeaderSubjectKey = @"Subject";
NSString * const MKMessageHeaderXMailTagsKey = @"X-MailTags";


