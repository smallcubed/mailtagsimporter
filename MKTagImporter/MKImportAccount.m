//
//  MKImportAccount.m
//  MKTagImporter
//
//  Created by Scott Morrison on 28/08/2018.
//  Copyright © 2018 SmallCubed. All rights reserved.
//

#import "MKImportAccount.h"
#import "NSString+MimeEncoding.h"


@implementation MKImportAccount
-(instancetype)initWithTagAccountIdentifier:(NSString*)identifier{
    self = [self init];
    if (self){
        self.tagAccountIdentifier = identifier;
    }
    return self;
}

-(instancetype)initWithAccountLoginName:(NSString*)loginName hostName:(NSString*)hostName{
      NSString * accountIdentifier = [[NSString stringWithFormat:@"%@@%@",[loginName lowercaseString],[hostName lowercaseString]] shortSHAHashString];
    return [self initWithTagAccountIdentifier:accountIdentifier];
    
}
@end
