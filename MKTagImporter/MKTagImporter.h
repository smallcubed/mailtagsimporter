//
//  MKTagImporter.h
//  MKTagImporter
//
//  Created by Scott Morrison on 28/08/2018.
//  Copyright © 2018 SmallCubed. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for MKTagImporter.
FOUNDATION_EXPORT double MKTagImporterVersionNumber;

//! Project version string for MKTagImporter.
FOUNDATION_EXPORT const unsigned char MKTagImporterVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MKTagImporter/PublicHeader.h>


#import "MKMessageHeaders.h"
#import "MKImportAccount.h"
#import "MKImportedTags.h"

