//
//  MKMessageHeaders.h
//  Martlet
//
//  Created by Scott Morrison on 11-01-30.
//  Copyright 2011 Indev Software, Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

/*
    MKMessageHeaders is a lightweight class that will read in the rfc822 data from a emlx file and store the header data in a dictionary of <NSString*, <NSArray <NSString*>* >
    Once the headers are read in you can request the "tagMessageIdentifier" as a property
    to get the full tagIdentifier (inclusive of accountIdentifier, use the -tagIdentifierForImportAccount: method
 
 */


extern NSString * const MKMessageHeaderDateKey;
extern NSString * const MKMessageHeaderFromKey;
extern NSString * const MKMessageHeaderSenderKey;
extern NSString * const MKMessageHeaderToKey;
extern NSString * const MKMessageHeaderCcKey;
extern NSString * const MKMessageHeaderBccKey;
extern NSString * const MKMessageHeaderMessageIdKey;
extern NSString * const MKMessageHeaderSubjectKey;
extern NSString * const MKMessageHeaderXMailTagsKey;

@class MKImportAccount;

@interface MKMessageHeaders : NSObject
@property (assign) NSStringEncoding preferredEncoding;
@property (readonly) NSDictionary * headerDictionary;
@property (readonly) NSString * tagMessageIdentifier;

+ (MKMessageHeaders *)headersFromEmlxFileAtPath:(NSString*)path;

- (id)initWithHeaderDictionary:(NSDictionary *)aHeaderDictionary;
- (NSString*)firstHeaderForKey:(NSString *)key;
- (NSString*)firstHeaderForKey:(NSString *)key decodeMime:(BOOL)decodeMime;

- (NSString*)tagIdentifierForImportAccount:(MKImportAccount*)account;

@end

