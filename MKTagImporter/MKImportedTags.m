//
//  MKImportedTags.m
//  MKTagImporter
//
//  Created by Scott Morrison on 28/08/2018.
//  Copyright © 2018 SmallCubed. All rights reserved.
//

#import "MKImportedTags.h"
@interface MKImportedTags()
@property (assign) NSInteger  postmarkSequenceNumber;
@property (copy) NSDate * modificationDate;
@end


@implementation MKImportedTags
+(instancetype)importedTagsForEmlxFile:(NSString*)emlxPath{
    NSString * tagIdentifier = [self tagIdentifierFromEmlxFile:emlxPath];
    MKImportedTags * tags = nil;
    if (tagIdentifier){
        tags = [[MKImportedTags alloc] initWithJsonFileForTagIdentifier:tagIdentifier];
    }
    return tags;
    
}
+(NSString*) tagIdentifierFromEmlxFile:(NSString*)emlxPath {

        int fd = open([emlxPath fileSystemRepresentation],O_RDONLY);
        if (fd<0){
            //NSLog(@"[MailTags] error reading cache file (can't open file)\n\t\tFile: %@\n\t\tMessage: %@",path,msg);
            return NULL;
        }
        
        char offsetBuffer[12];
        if (read(fd,offsetBuffer,11)!=11){
            NSLog(@"Error reading cache file (can't read plist offset)\n\t\tFile: %@",emlxPath);
            close(fd);
            return NULL;
        }
        offsetBuffer[11] = 0;
        
        long offset = atol(offsetBuffer);
        char *newLinePtr = strchr(offsetBuffer,'\n');
        if (!newLinePtr){
            NSLog(@"Error reading cache file (plist offset too large)\n\t\tFile: %@",emlxPath);
            close(fd);
            return NULL;
        }
        off_t plist_offset = (newLinePtr - offsetBuffer) + offset + 1;
        off_t file_size = lseek(fd,0,SEEK_END);
        if (plist_offset>file_size){
            NSLog(@"Error reading cache file (plist offset is beyond file size: %lld)\n\t\tFile: %@",plist_offset,emlxPath);
            
            close(fd);
            return NULL;
        }
        
        off_t seeked_offset = lseek(fd,plist_offset,SEEK_SET);
        if (seeked_offset != plist_offset){
            NSLog(@"Error reading cache file (can't seek plist: %lld)\n\t\tFile: %@",plist_offset,emlxPath);
            close(fd);
            return NULL;
        }
        
        NSInteger plist_length = file_size - plist_offset;
        if (plist_length<0){
            NSLog(@"Errorr reading cache file (plist data length inconsistent: %lld)\n\t\tFile: %@",(long long)plist_length,emlxPath);
            close(fd);
            return NULL;
        }
        
        NSMutableData *data = [NSMutableData dataWithLength: plist_length];
        if (read(fd,[data mutableBytes],plist_length)!=plist_length){
            NSLog(@"[MailTags] error reading cache file (plist data length inconsistent: %lld)\n\t\tFile: %@",(long long)plist_length,emlxPath);
            close(fd);
            return NULL;
        }
        close(fd);
    NSError * plistError = nil;
    NSDictionary * dictionary = [NSPropertyListSerialization propertyListWithData:data options:0 format:nil error:&plistError];
    return dictionary[@"com.smallcubed.tagIdentifier"];
}

-(instancetype)initWithJsonFileForTagIdentifier:(NSString *)tagIdentifier{
    NSString * rootPath = [@"~/Library/Mail/SmallCubed/TagData" stringByStandardizingPath];
    return [self initWithJsonFileForTagIdentifier:tagIdentifier relativeToPath:rootPath];
}

-(instancetype)initWithJsonFileForTagIdentifier:(NSString*)tagIdentifier relativeToPath:(NSString*)rootPath{
    self = [self init];
    if (self){
        // break the tagIdentifier identifier into parts and form a jsonFilePath
        self.tagIdentifier = tagIdentifier;
        
        NSArray * pmPart = [tagIdentifier componentsSeparatedByString:@"/"];
        NSString * accountPath = [rootPath stringByAppendingPathComponent:pmPart[0]];
        NSMutableString * tagIdentifierFolderPath = [accountPath mutableCopy];
        NSString * messageIdentifier = pmPart[1];
        for (int idx = 0; idx<4; idx++) {
            [tagIdentifierFolderPath appendFormat:@"/%c", [messageIdentifier characterAtIndex:idx]];
        }
        NSString * jsonFilePath = [[tagIdentifierFolderPath stringByAppendingPathComponent:messageIdentifier] stringByAppendingPathExtension:@"MKTag"];
        
        // read the json data
    
        NSData * jsonData = [NSData dataWithContentsOfFile:jsonFilePath];
        if (jsonData){
            NSError * jsonError = nil;
            NSDictionary * dictRep = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error: &jsonError];
            if (dictRep && !jsonError){
                [self parseDictionaryRepresentation:dictRep];
            }
        }
    }
    return self;
}
+(NSString*)jsonForTagIdentifier:(NSString*) tagIdentifier relativeToPath:(NSString*)rootPath{
    NSArray * pmPart = [tagIdentifier componentsSeparatedByString:@"/"];
    NSString * accountPath = [rootPath stringByAppendingPathComponent:pmPart[0]];
    NSMutableString * tagIdentifierFolderPath = [accountPath mutableCopy];
    NSString * messageIdentifier = pmPart[1];
    for (int idx = 0; idx<4; idx++) {
        [tagIdentifierFolderPath appendFormat:@"/%c", [messageIdentifier characterAtIndex:idx]];
    }
    NSString * jsonFilePath = [[tagIdentifierFolderPath stringByAppendingPathComponent:messageIdentifier] stringByAppendingPathExtension:@"MKTag"];
    
    // read the json data
    
     NSData * jsonData = [NSData dataWithContentsOfFile:jsonFilePath];
    if (jsonData){
        NSError * jsonError = nil;
        NSDictionary * dictRep = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error: &jsonError];
        if (dictRep && !jsonError){
            NSDictionary * tags =  dictRep[@"TAGS"];
            if (tags){
                NSData * justTagsData = [NSJSONSerialization dataWithJSONObject:tags options:0 error:&jsonError];
                return [[NSString alloc] initWithData:justTagsData encoding:NSUTF8StringEncoding];
            }
        }
    }
    return @"{}";
}
-(void)parseDictionaryRepresentation:(NSDictionary*)jsonRep{
    
    // set only the internal values to prevent modification date changes
    NSDictionary * tagJsonRep = jsonRep[@"TAGS"];
    
    self.keywords = tagJsonRep[MKTagDataKeyKeyword];
    self.project  = tagJsonRep[MKTagDataKeyProject];
    self.tickleDate = [NSDate dateWithTimeIntervalSince1970:[tagJsonRep[MKTagDataKeyTickleDate] floatValue]];
    self.notes = tagJsonRep[MKTagDataKeyNote];
    self.importance = tagJsonRep[MKTagDataKeyImportance];
    self.showNoteAsSubject = [tagJsonRep[MKTagDataKeyNoteAsSubject] boolValue];
    self.color = tagJsonRep[MKTagDataKeyColor];
    // tasks and events not implemented yet due to their more complex structure
    //self.tasks = rep[MKTagDataKeyTasks];
    //self.events = rep[MKTagDataKeyEvents];
    
    self.postmarkSequenceNumber = [jsonRep[MKTagDataKeyPostmarkSequence] integerValue];
    self.modificationDate = jsonRep[MKTagDataKeyModificationDate];
}


@end

NSString* const MKTagDataKeyImportance                = @"im";
NSString* const MKTagDataKeyKeyword                   = @"kw";
NSString* const MKTagDataKeyProject                   = @"pr";
NSString* const MKTagDataKeyTickleDate                = @"tk";
NSString* const MKTagDataKeyNote                      = @"nt";
NSString* const MKTagDataKeyNoteAsSubject             = @"ns";
NSString* const MKTagDataKeyAlternateSubject          = @"as";
NSString* const MKTagDataKeyFlag                      = @"fl";
NSString* const MKTagDataKeyTasks                     = @"i_ts";
NSString* const MKTagDataKeyEvents                    = @"i_ev";
NSString* const MKTagDataKeyColor                     = @"mc";

// metadata keys
NSString* const MKTagDataKeyModificationDate          = @"MD";
NSString* const MKTagDataKeyPostmarkSequence          = @"SN";

