//
//  MKImportedTags.h
//  MKTagImporter
//
//  Created by Scott Morrison on 28/08/2018.
//  Copyright © 2018 SmallCubed. All rights reserved.
//

#import <Cocoa/Cocoa.h>


NS_ASSUME_NONNULL_BEGIN



@interface MKImportedTags : NSObject

@property (copy) NSString * tagIdentifier;

@property (copy) NSArray <NSString*> * keywords;
@property (copy) NSString * project;
@property (copy) NSNumber * importance;
@property (copy) NSDate * tickleDate;
@property (copy) NSColor * color;
@property (copy) NSString * notes;

@property (assign) BOOL showNoteAsSubject;

@property (copy) NSArray * tasks;
@property (copy) NSArray * events;

+(instancetype)importedTagsForEmlxFile:(NSString*)emlxPath;

// returns tagData with identifier relative to ~/Library/Mail/SmallCubed/TagData  (the default path)
-(instancetype)initWithJsonFileForTagIdentifier:(NSString *)tagIdentifier;
-(instancetype)initWithJsonFileForTagIdentifier:(NSString*)tagIdentifier relativeToPath:(NSString*)rootPath;
+(NSString*) tagIdentifierFromEmlxFile:(NSString*)emlxPath;
+(NSString*)jsonForTagIdentifier:(NSString*) tagIdentifier relativeToPath:(NSString*)rootPath;
@end


extern NSString* const MKTagDataKeyImportance;
extern NSString* const MKTagDataKeyKeyword;
extern NSString* const MKTagDataKeyProject;
extern NSString* const MKTagDataKeyTickleDate;
extern NSString* const MKTagDataKeyNote;
extern NSString* const MKTagDataKeyNoteAsSubject;
extern NSString* const MKTagDataKeyAlternateSubject;
extern NSString* const MKTagDataKeyFlag;
extern NSString* const MKTagDataKeyTasks;
extern NSString* const MKTagDataKeyEvents;
extern NSString* const MKTagDataKeyColor;

// metadata keys
extern NSString* const MKTagDataKeyModificationDate;
extern NSString* const MKTagDataKeyPostmarkSequence;



NS_ASSUME_NONNULL_END
