//
//  main.m
//  TagTool
//
//  Created by Scott Morrison on 15/09/2018.
//  Copyright © 2018 SmallCubed. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MKImportedTags.h"
int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...
       
        NSArray * args =  [[NSProcessInfo processInfo] arguments];
        if (args.count>1){
            NSString * tagIdentifier = [MKImportedTags tagIdentifierFromEmlxFile:args[1]];
            if (tagIdentifier){
                NSString * rootPath = [@"~/Library/Mail/SmallCubed/TagData" stringByStandardizingPath];
                NSString * json = [MKImportedTags jsonForTagIdentifier:tagIdentifier relativeToPath:rootPath];
                if (json){
                    printf("%s\n",(const char*)[json UTF8String]);
                }
                else{
                    printf("{}\n");
                }
            }
        }
        else{
             NSLog(@"");
        }
       
    }
    return 0;
}
